# Docker image: Singlarity 3.5.3 + Miniconda3 + Snakemake 5.9.1

[![Docker Repository on Quay](https://quay.io/repository/sebio/docker-snakemake/status "Docker Repository on Quay")](https://quay.io/repository/sebio/docker-snakemake)

Mainly used for CI of Snakemake workflows using singularity v3.

Hosted on quay.io:

```bash
docker pull quay.io/sebio/docker-snakemake
```
