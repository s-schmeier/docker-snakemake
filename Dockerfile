FROM continuumio/miniconda3
MAINTAINER Sebastian Schmeier <s.schmeier@pm.me>

ENV LANG C.UTF-8
ENV SHELL /bin/bash

RUN conda config --add channels defaults && \
    conda config --add channels bioconda && \
    conda config --add channels conda-forge
RUN conda update -n base -c defaults conda && \
    conda info --all

RUN conda install -y singularity=3.5.3 && \
    conda install -y -c conda-forge/label/gcc7 singularity=3.5.3 && \
    conda install -y -c conda-forge/label/cf201901 singularity=3.5.3
RUN singularity --version
RUN conda create -y -n snakemake snakemake=5.9.1 python=3.6
RUN /bin/bash -c "source activate snakemake && snakemake --version"
RUN conda clean --all -y
ENV PATH="/opt/conda/envs/snakemake/bin:${PATH}"
